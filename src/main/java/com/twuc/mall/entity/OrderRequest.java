package com.twuc.mall.entity;

public class OrderRequest {
    private long goodsId;
    private int num;

    public OrderRequest() {
    }

    public OrderRequest(long goodsId, int num) {
        this.goodsId = goodsId;
        this.num = num;
    }
}
