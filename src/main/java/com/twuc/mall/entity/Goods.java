package com.twuc.mall.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long goodsId;
    @NotNull
    private String name;
    @NotNull
    private long price;
    @NotNull
    private String unit;
    @NotNull
    private String img;

    //@ManyToOne
    //private Orders orders;

    public Goods() {
    }


    public Goods(String name, long price, String unit, String img) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.img = img;
    }

    public void setGoodsId(long goodsId) {
        this.goodsId = goodsId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public long getGoodsId() {
        return goodsId;
    }

    public String getName() {
        return name;
    }

    public long getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImg() {
        return img;
    }
}
