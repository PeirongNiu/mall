package com.twuc.mall.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long orderId;
    private int num;
    private long goodsId;
    //@OneToMany
    //private List<Goods> goodses=new ArrayList<>();

    public Orders() {
    }

    public Orders(long goodsId, int num) {
        this.num = num;
        this.goodsId = goodsId;
    }

    public void setOrdId(long ordId) {
        this.orderId = ordId;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setGoodsId(long goodsId) {
        this.goodsId = goodsId;
    }



    public long getOrdId() {
        return orderId;
    }

    public int getNum() {
        return num;
    }

    public long getGoodsId() {
        return goodsId;
    }


}
