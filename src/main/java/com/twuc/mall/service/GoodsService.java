package com.twuc.mall.service;

import com.twuc.mall.dao.GoodsRepository;
import com.twuc.mall.entity.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsService {
    @Autowired
    private GoodsRepository goodsRepository;

    public void createGoods(Goods goods) {
        if (goodsRepository.findByName(goods.getName()).size() > 0)
            throw new RuntimeException("商品名称已存在，请重新输入新的商品");
        Goods save = goodsRepository.save(goods);
    }

    public List<Goods> getGoods() {
        List<Goods> allGoods = goodsRepository.findAll();
        if (allGoods.size()==0)
            return null;
        return allGoods;
    }
}
