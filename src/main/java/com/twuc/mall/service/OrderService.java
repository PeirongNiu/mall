package com.twuc.mall.service;

import com.twuc.mall.dao.OrderRepository;
import com.twuc.mall.entity.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public void createOrder(Orders order) {
        orderRepository.save(order);
    }

    public List<Orders> getOrder() {
        List<Orders> allOrder = orderRepository.findAll();
        return allOrder;
    }
}
