package com.twuc.mall.dao;

import com.twuc.mall.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Orders, Long> {
    Orders findByGoodsId(long goodsId);
}
