package com.twuc.mall.web;

import com.twuc.mall.entity.Orders;
import com.twuc.mall.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/mall/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping
    public ResponseEntity createOrder(@RequestBody Orders order) {
        orderService.createOrder(order);
        return ResponseEntity.status(201).build();
    }

    @GetMapping
    public ResponseEntity getOrder(){
        List<Orders> order = orderService.getOrder();
        return ResponseEntity.ok(order);
    }
}
