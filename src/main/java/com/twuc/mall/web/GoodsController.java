package com.twuc.mall.web;

import com.twuc.mall.entity.Goods;
import com.twuc.mall.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/mall")
@CrossOrigin(origins = "*")
public class GoodsController {

    @Autowired
    GoodsService goodsService;

    @PostMapping("/goods")
    public ResponseEntity createGoods(@RequestBody @Valid Goods goods){
        goodsService.createGoods(goods);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/goods")
    public ResponseEntity getGoods(){
        List<Goods> goods = goodsService.getGoods();
        if (goods==null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok(goods);
    }
}
