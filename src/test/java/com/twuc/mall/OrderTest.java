package com.twuc.mall;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.mall.dao.OrderRepository;
import com.twuc.mall.entity.Orders;
import org.hibernate.criterion.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class OrderTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private OrderRepository orderRepository;

    @Test
    void should_create_order() throws Exception {
        Orders order = new Orders(1L, 1);
        ObjectMapper objectMapper = new ObjectMapper();
        String orderString = objectMapper.writeValueAsString(order);
        mockMvc.perform(post("/mall/order")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .content(orderString))
                .andExpect(status().isCreated());
    }

    @Test
    void should_get_order() throws Exception {
        orderRepository.save(new Orders(1L, 1));
        mockMvc.perform(get("/mall/order"))
                .andExpect(status().isOk());
    }
}
