package com.twuc.mall;

import com.twuc.mall.dao.GoodsRepository;
import com.twuc.mall.entity.Goods;
import com.twuc.mall.web.GoodsController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
@ActiveProfiles("test")
public class GoodsJpaTest {
    @Autowired
    private GoodsRepository goodsRepository;
    @Autowired
    GoodsController goodsController;

//    @Test
//    void should_create_goods_success_in_jpa() {
//        Goods goods = new Goods("可乐", 3L, "瓶", "tp");
//        goodsController.createGoods(goods);
//        Optional<Goods> byId = goodsRepository.findById(goods.getGoodsId());
//        assertEquals(goods,byId.get());
//    }
}
