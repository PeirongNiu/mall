package com.twuc.mall;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.mall.dao.GoodsRepository;
import com.twuc.mall.entity.Goods;
import com.twuc.mall.web.GoodsController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sun.security.krb5.internal.crypto.Nonce.value;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class GoodsTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private GoodsRepository goodsRepository;


    @Test
    void should_create_goods_return_201() throws Exception {
        Goods goods = new Goods("可乐", 3L, "瓶", "tp");
        ObjectMapper objectMapper = new ObjectMapper();
        String goodsString = objectMapper.writeValueAsString(goods);
        mockMvc.perform(post("/mall/goods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(goodsString))
                .andExpect(status().isCreated());
    }


    @Test
    void should_get_goods_return_200() throws Exception {
        Goods goods = new Goods("可乐", 3L, "瓶", "tp");
        Goods save = goodsRepository.save(goods);
        mockMvc.perform(get("/mall/goods"))
                .andExpect(status().isOk());
//        .andExpect(jsonPath("$.name").value(save.getName()))
//        .andExpect(jsonPath("$.price").value(save.getPrice()))
//        .andExpect(jsonPath("$.unit").value(save.getUnit()))
//        .andExpect(jsonPath("$.img").value(save.getImg()));
    }
}
